package com.example.amore;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class OnboardingActivity extends AppCompatActivity {

    private Button button;
    private ImageView animatedImage;
    private ImageView animatedImage2;
    private ImageView animatedImage3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding);

        getSupportActionBar().hide();

        animatedImage = findViewById(R.id.obpic);
        animatedImage2 = findViewById(R.id.obtext);
        animatedImage3 = findViewById(R.id.bodyobp);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_in);
        Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);

        animatedImage.startAnimation(animation);
        animatedImage2.startAnimation(animation2);
        animatedImage3.startAnimation(animation);

        button = findViewById(R.id.button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openOnboardingActivity2();
            }
        });
    }

    private void openOnboardingActivity2() {
        Intent intent = new Intent(this, OnboardingActivity2.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}