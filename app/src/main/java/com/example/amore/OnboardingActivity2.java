package com.example.amore;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class OnboardingActivity2 extends AppCompatActivity {

    private Button signinButton, signupButton;
    private ImageView animatedImage, animatedImage2, animatedImage3, animatedImage4, animatedImage5, animatedImage6, animatedImage7, animatedImage8;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_onboarding2);
        getSupportActionBar().hide();


        signinButton = findViewById(R.id.signinbtn);
        signupButton = findViewById(R.id.signupbtn);
        animatedImage = findViewById(R.id.ob2pic);
        animatedImage2 = findViewById(R.id.ob2h);
        animatedImage3 = findViewById(R.id.film);
        animatedImage4 = findViewById(R.id.music);
        animatedImage5 = findViewById(R.id.mtext);
        animatedImage6 = findViewById(R.id.mlogo);
        animatedImage7 = findViewById(R.id.filmtext);
        animatedImage8 = findViewById(R.id.filmlogo);

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSignInActivity();
            }
        });

        signupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSignUpActivity();
            }
        });

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_in);
        Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);


        animatedImage.startAnimation(animation);
        animatedImage2.startAnimation(animation2);
        animatedImage3.startAnimation(animation);
        animatedImage4.startAnimation(animation2);
        animatedImage5.startAnimation(animation2);
        animatedImage6.startAnimation(animation2);
        animatedImage7.startAnimation(animation2);
        animatedImage8.startAnimation(animation);
    }

    private void openSignInActivity() {
        Intent intent = new Intent(this, Sign_in.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }

    private void openSignUpActivity() {
        Intent intent = new Intent(this, Sign_up.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}