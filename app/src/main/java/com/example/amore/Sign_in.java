package com.example.amore;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class Sign_in extends AppCompatActivity {

    private Button signinButton;
    private ImageView animatedImage;
    private ImageView animatedImage2;
    private ImageView animatedImage4;
    private ImageView animatedImage3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        getSupportActionBar().hide();

        signinButton = findViewById(R.id.buttonSignIn);
        animatedImage = findViewById(R.id.wb);
        animatedImage2 = findViewById(R.id.phinmas);
        animatedImage3 = findViewById(R.id.emailText);
        animatedImage4 = findViewById(R.id.passwordText);

        signinButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Get user input
                String email = ""; // Replace with your code to get the email
                String password = ""; // Replace with your code to get the password

                // Validate email and password
                if (isInputValid(email, password)) {
                    // Replace with your actual sign-in logic
                    boolean isSignInSuccessful = performSignIn();

                    if (isSignInSuccessful) {
                        openStudentDataActivity();
                    } else {
                        // Show an error message
                        Toast.makeText(Sign_in.this, "Sign-in failed. Please check your credentials.", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    // Show an error message
                    Toast.makeText(Sign_in.this, "Please enter both email and password.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_in);
        Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);

        animatedImage.startAnimation(animation);
        animatedImage2.startAnimation(animation2);
        animatedImage3.startAnimation(animation2);
        animatedImage4.startAnimation(animation2);
    }

    // Replace this with your actual sign-in logic
    private boolean performSignIn() {
        // Replace this with your authentication logic
        // For example, you can check email and password against a database
        // If the credentials are correct, return true; otherwise, return false
        return true; // Successful sign-in for this example
    }

    // Add a validation check for email and password
    private boolean isInputValid(String email, String password) {
        return !email.isEmpty() && !password.isEmpty();
    }

    private void openStudentDataActivity() {
        Intent intent = new Intent(this, studentData.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }
}