package com.example.amore;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class Sign_up extends AppCompatActivity {

    private ImageView animatedImage, animatedImage2, animatedImage3, animatedImage4;
    private Button Buttonsignup;

    @SuppressLint({"WrongViewCast", "MissingInflatedId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        animatedImage = findViewById(R.id.createPic);
        animatedImage2 = findViewById(R.id.emailText);
        animatedImage3 = findViewById(R.id.passwordText);
        animatedImage4 = findViewById(R.id.confirmPic);
        Buttonsignup = findViewById(R.id.buttonSignup);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);
        Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.slide_in);

        animatedImage.startAnimation(animation2);
        animatedImage2.startAnimation(animation);
        animatedImage3.startAnimation(animation);
        animatedImage4.startAnimation(animation);

        Buttonsignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Call the method to open the Student_Data activity
                openStudentDataActivity();
            }
        });
    }

    private void openStudentDataActivity() {
        Intent intent = new Intent(Sign_up.this, studentData.class);
        startActivity(intent);
        // Apply transition animation
        overridePendingTransition(R.anim.fade_in ,R.anim.fade_out);
    }
}
