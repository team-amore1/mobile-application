package com.example.amore;

import androidx.appcompat.app.AppCompatActivity;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class studentData extends AppCompatActivity {

    private ImageView animatedImage, animatedImage2, animatedImage3, animatedImage4, animatedImage5;
    private Button nextbtn;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.studentdata);
        getSupportActionBar().hide();

        nextbtn = findViewById(R.id.btnNext);
        animatedImage = findViewById(R.id.studentData);
        animatedImage2 = findViewById(R.id.sDp);
        animatedImage3 = findViewById(R.id.studentId);
        animatedImage4 = findViewById(R.id.major);
        animatedImage5 = findViewById(R.id.department);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_up);
        Animation animation2 = AnimationUtils.loadAnimation(this, R.anim.slide_in_left);

        animatedImage.startAnimation(animation2);
        animatedImage2.startAnimation(animation2);
        animatedImage3.startAnimation(animation2);
        animatedImage4.startAnimation(animation2);
        animatedImage5.startAnimation(animation2);


        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Call the method to open the studentData activity
                openNameActivity();
            }
        });
    }

    private void openNameActivity() {
        Intent intent = new Intent(this, studentname.class);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out);
    }
}
