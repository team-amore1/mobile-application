package com.example.amore;

import static com.example.amore.R.id.whatGender;
import static com.example.amore.R.id.whatage;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;

public class studentgender extends AppCompatActivity {

    private ImageView animatedImage;
    private Button nextbtn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studentgender);
        getSupportActionBar().hide();

        nextbtn = findViewById(R.id.btnNext);
        animatedImage = findViewById(whatGender);

        Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_in);

        animatedImage.startAnimation(animation);

        nextbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openStudentInterestActivity();
            }
        });
    }

    private void openStudentInterestActivity() {
        Intent intent = new Intent(this, studentinterest.class);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out);
    }
}
